import React, { Component } from 'react';
import { Card, CardTitle, CardText, CardActions } from 'react-md/lib/Cards';
import Button from 'react-md/lib/Buttons/Button';
import SelectField from 'react-md/lib/SelectFields';
import TextField from 'react-md/lib/TextFields';
import axios from 'axios';
import { parseString } from 'xml2js';
import _ from 'lodash';

import './App.css';

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      items: [],
      currentPage: 0,
      itemsPerPage: 3,
      url: '',
      error: false,
    };

    this.next = this.next.bind(this);
    this.back = this.back.bind(this);
    this.handleInput = this.handleInput.bind(this);
    this.renderPagination = this.renderPagination.bind(this);
    this.loadData = this.loadData.bind(this);
  }

  componentDidMount() {
    this.loadData();
  }

  loadData() {
    const { url } = this.state;

    axios.get(url || 'http://fapl.ru/rss.xml')
      .then(res => {
        parseString(res.data, (err, result) => {
          const items = _.result(result, 'rss.channel[0].item', []);
          this.setState({ items, error: _.isEmpty(items) ? true : false });
        })
      })
      .catch(error => this.setState({ items: [], error: true }));
  }

  next() {
    this.setState(prevState => ({ currentPage: prevState.currentPage + 1 }))
  }

  back() {
    this.setState(prevState => ({ currentPage: prevState.currentPage - 1 }))
  }

  handleInput(name, value) {
    this.setState({ [name]: value, currentPage: 0 });
  }

  renderPagination() {
    const { items, currentPage, itemsPerPage } = this.state;

    return (
      <div className="pagination">
        <Button
          icon
          primary
          onClick={this.back}
          disabled={currentPage === 0}
        >
          navigate_before
        </Button>
        <SelectField
          id="itemsPerPage"
          value={itemsPerPage}
          onChange={value => this.handleInput('itemsPerPage', value)}
          menuItems={_.range(1, 11)}
          position={SelectField.Positions.TOP_RIGHT}
        />
        <Button
          icon
          primary
          onClick={this.next}
          disabled={itemsPerPage >= items.length || currentPage * (itemsPerPage + 1) >= items.length}
        >
          navigate_next
        </Button>
      </div>
    );
  }

  render() {
    const { items, currentPage, itemsPerPage, url, error } = this.state;
    const filteredItems = _.slice(items, currentPage * itemsPerPage, currentPage * itemsPerPage + itemsPerPage);

    return (
      <div className="md-grid app-container">
        <div className="md-cell md-cell--6-desktop md-cell--3-desktop-offset">
          <TextField
            id="url"
            placeholder="http://fapl.ru/rss.xml"
            size={10}
            value={url}
            onChange={value => this.handleInput('url', value)}
            customSize="title"
            lineDirection="right"
          />
          <Button
            raised
            secondary
            disabled={!url}
            label="Показать"
            onClick={this.loadData}
          />
          {
            error ?
              <div className="md-headline md-text-center">Произошла ошибка</div>
            : null
          }
          {
            filteredItems.map((item, index) =>
              <Card raise className="app-card" key={index}>
                <CardTitle title={item.title} subtitle={item.pubDate} />
                <CardText dangerouslySetInnerHTML={{__html: item.description}} />
                <CardActions>
                  <Button
                    raised
                    primary
                    label="Перейти"
                    href={item.link[0]}
                    className="md-cell--right"
                    target="_blank"
                  />
                </CardActions>
              </Card>
            )
          }
          {this.renderPagination()}
        </div>
      </div>
    );
  }
}

export default App;
